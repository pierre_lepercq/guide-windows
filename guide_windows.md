# Comprendre le fonctionnement d'un ordinateur et comment bien s'en servir

## 1- Les Systèmes d'exploitation
Un ordinateur est avant tout une machine, capable d'enregistrer des données, de réaliser de nombreux calculs très complexes, et de communiquer avec d'autres machines. 
Le système d'exploitation (aussi appelé OS) est un ensemble de programmes qui dirige l'utilisation des ressources d'un ordinateur. C'est le système d'exploitation qui permet l'échange entre l'utilisateur et la machine. Ces programmes ont tous un rôle bien précis comme:
- La gestion des fichiers
- La lecture, l'édition et la sauvegarde de documents
- L'affichage et la gestion graphique des fenêtres, du bureau, et des espaces de travail (Interface)
- L'accès au réseau et à internet
- ...
Il existe plusieurs systèmes d'exploitation comme Windows, MacOS, Linux. Chacun vient avec son ensemble de programmes relatif à l'expérience utilisateur qu'il propose, en faisant varier l'affichage et l'accès aux différentes fonctionnalités. 
il est important de comprendre que les systèmes d'exploitation concernent aussi bien les ordinateurs, que les smartphones, tablettes et autres appareils High-Tech. Les programmes peuvent donc varier en fonction de la platform mais le fonctionnement tend à s'uniformiser.
Pour la suite, je vais me concentrer sur le système d'exploitation Windows qui est le plus couramment utilisé.

## 2- Le système d'exploitation Windows
Je vais ici tenter de présenter au mieux le système d'exploitation Windows. Comme tout OS, il se renouvèle pour proposer une expérience plus proche des besoins utilisateur à chaque nouvelle version. Cela peut parraître confusant, mais ces dernières apportent également leur lot de nouvelles fonctionnalités permettant une expérience plus personnelle de la machine.
Les différents logiciels de base à connaître sont :
- explorateur de fichiers : permet de parcourir l'ensemble des fichiers contenu sur les différents périphériques de stockage de l'ordinateur.
- panneau de configuration : permet de configurer l'ordinateur (admnistration des comptes, apparence, gestion des périphériques, ...)

### a- 









